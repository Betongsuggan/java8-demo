package com.findwise.java8demo.optionals;

import com.findwise.java8demo.person.Person;

public class NullableOperations {

    private Person person;

    public NullableOperations(Person person) {
        this.person =  person;
    }

    public String getFullName() {
        String firstName = person.getFirstName();
        String lastName = person.getLastName();

        if (firstName == null || lastName == null) {
            return null;
        }
        else {
            return firstName + " " + lastName;
        }
    }
}
