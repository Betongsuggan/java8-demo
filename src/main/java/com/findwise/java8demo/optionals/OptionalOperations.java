package com.findwise.java8demo.optionals;

import com.findwise.java8demo.person.Person;

import java.util.Optional;

public class OptionalOperations {
    private Person person;

    public OptionalOperations(Person person) {
        this.person = person;
    }

    public Optional<String> getFullName() {
        Optional<String> optFirstName = Optional.ofNullable(person.getFirstName());
        Optional<String> optLastName = Optional.ofNullable(person.getLastName());

        return optFirstName
                .flatMap(firstName -> optLastName.map(lastName -> firstName + " " + lastName));
    }
}
