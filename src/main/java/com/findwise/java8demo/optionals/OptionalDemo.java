package com.findwise.java8demo.optionals;

import com.findwise.java8demo.person.Person;

public class OptionalDemo {

    private NullableOperations nullableOperations;
    private OptionalOperations optionalOperations;

    public OptionalDemo() {
        Person person = getPerson();

        nullableOperations = new NullableOperations(person);
        optionalOperations = new OptionalOperations(person);
    }

    private void doOptionalOperations() {
        optionalOperations.getFullName()
                .map(String::toLowerCase)
                .ifPresent(System.out::println);
    }

    private void doNullableOperations() {
        String fullName = nullableOperations.getFullName();
        System.out.println(fullName.toLowerCase());
    }

    private Person getPerson() {
        Person person = new Person();
        person.setLastName("Janus");
        person.setAge(20);
        person.setShoeSize(36.5);

        return person;
    }

    public static void main(String[] args) {
        OptionalDemo demo =new OptionalDemo();

        demo.doOptionalOperations();
        demo.doNullableOperations();
    }
}
