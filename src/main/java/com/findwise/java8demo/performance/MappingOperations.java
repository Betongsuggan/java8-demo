package com.findwise.java8demo.performance;

import com.findwise.java8demo.person.Person;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MappingOperations implements Operations {

    @Override
    public void doJava7Operations(List<Person> persons) {

        Map<String, Double> averageAgesOfFavouriteAnimalsJava7 = new HashMap<>();
        Map<String, Integer> tmp = new HashMap<>();

        for (Person person : persons) {
            if (!tmp.containsKey(person.getFavouriteAnimal())) {
                tmp.put(person.getFavouriteAnimal(), person.getAge());
            } else {
                Integer totalAge = tmp.get(person.getFavouriteAnimal());
                tmp.put(person.getFavouriteAnimal(), totalAge + person.getAge());
            }
        }

        for (Map.Entry<String, Integer> animalToTotalAge : tmp.entrySet()) {
            double averageAge = animalToTotalAge.getValue() / persons.size();
            averageAgesOfFavouriteAnimalsJava7.put(animalToTotalAge.getKey(), averageAge);
        }
    }

    @Override
    public void doStreamOperations(List<Person> persons) {
        persons.stream()
                .collect(Collectors.groupingBy(Person::getFavouriteAnimal, Collectors.averagingDouble(Person::getAge)));
    }

    @Override
    public void doParallelOperations(List<Person> persons) {
        persons.parallelStream()
                .collect(Collectors.groupingBy(Person::getFavouriteAnimal, Collectors.averagingDouble(Person::getAge)));
    }

}
