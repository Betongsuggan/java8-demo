package com.findwise.java8demo.performance;

import com.findwise.java8demo.person.Person;

import java.util.List;

public interface Operations {
    void doJava7Operations(List<Person> persons);
    void doStreamOperations(List<Person> persons);
    void doParallelOperations(List<Person> persons);
}
