package com.findwise.java8demo.performance;

import com.findwise.java8demo.person.Person;
import com.findwise.java8demo.person.PersonGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class StreamPerformance {
    private final static Logger logger = LoggerFactory.getLogger(StreamPerformance.class);

    public StreamPerformance() {
        List<Person> randomPersons = new PersonGenerator().getRandomPersons(50000000);

        logPerformance(randomPersons, new MappingOperations());
    }

    private void logPerformance(List<Person> persons, Operations operations) {
        long nanoTimeStart = System.nanoTime();
        operations.doJava7Operations(persons);
        long nanoTimeEnd = System.nanoTime();
        logger.info("Java 7 remapping took " + (nanoTimeEnd-nanoTimeStart)/1000000 + " ms");

        nanoTimeStart = System.nanoTime();
        operations.doStreamOperations(persons);
        nanoTimeEnd = System.nanoTime();
        logger.info("Single threaded stream remapping took " + (nanoTimeEnd-nanoTimeStart)/1000000 + " ms");

        nanoTimeStart = System.nanoTime();
        operations.doParallelOperations(persons);
        nanoTimeEnd = System.nanoTime();
        logger.info("Parallell stream remapping took " + (nanoTimeEnd-nanoTimeStart)/1000000 + " ms");
    }

    public static void main(String[] args) {
        new StreamPerformance();
    }
}
