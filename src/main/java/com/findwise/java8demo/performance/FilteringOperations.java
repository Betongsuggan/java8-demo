package com.findwise.java8demo.performance;

import com.findwise.java8demo.person.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilteringOperations implements Operations {
    @Override
    public void doJava7Operations(List<Person> persons) {
        List<Person> filteredPersons = new ArrayList<>();

        for (Person person : persons) {
            if (person.getAge() < 50 && person.getShoeSize() > 40.0) {
                filteredPersons.add(person);
            }
        }
    }

    @Override
    public void doStreamOperations(List<Person> persons) {
        persons.stream()
                .filter(person -> person.getAge() < 50)
                .filter(person -> person.getShoeSize() > 40)
                .collect(Collectors.toList());
    }

    @Override
    public void doParallelOperations(List<Person> persons) {
        persons.parallelStream()
                .filter(person -> person.getAge() < 50)
                .filter(person -> person.getShoeSize() > 40)
                .collect(Collectors.toList());
    }
}
