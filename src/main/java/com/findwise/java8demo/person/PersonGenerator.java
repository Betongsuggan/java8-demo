package com.findwise.java8demo.person;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class PersonGenerator {

    private static final Random random = new Random();

    private List<String> firstNames;
    private List<String> lastNames;
    private List<String> animals;
    private List<String> carpets;

    public PersonGenerator() {
        firstNames = getNamesFromFile(getClass().getClassLoader().getResource("firstnames.txt"));
        lastNames = getNamesFromFile(getClass().getClassLoader().getResource("lastnames.txt"));
        animals = getNamesFromFile(getClass().getClassLoader().getResource("animals.txt"));
        carpets = getNamesFromFile(getClass().getClassLoader().getResource("carpets.txt"));
    }

    private List<String> getNamesFromFile(URL sourceFile) {
        try (BufferedReader lineReader = new BufferedReader(new FileReader(sourceFile.getPath()))) {
            return lineReader.lines()
                    .collect(Collectors.toList());
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

    private Person generateRandomPerson() {
        Person randomPerson = new Person();
        randomPerson.setFirstName(firstNames.get((int) (Math.random() * firstNames.size())));
        randomPerson.setLastName(lastNames.get((int) (Math.random() * lastNames.size())));
        randomPerson.setFavouriteAnimal(animals.get((int) (Math.random() * animals.size())));
        randomPerson.setAge((int) (Math.random() * 120));
        randomPerson.setShoeSize((Math.random() * 20) + 30);
        randomPerson.setGender(Gender.getRandomGender());
        randomPerson.setFavouriteCarpets(getRandomCarpets());

        return randomPerson;
    }

    public List<Person> getRandomPersons(long numberOfRandomPersons) {
        return LongStream.range(0, numberOfRandomPersons).parallel().boxed()
                .map(index -> generateRandomPerson())
                .collect(Collectors.toList());
    }

    private List<String> getRandomCarpets() {
        int size = random.nextInt(5);
        return IntStream.range(0, size).boxed()
                .map(integer -> carpets.get(random.nextInt(carpets.size())))
                .collect(Collectors.toList());
    }
}
