package com.findwise.java8demo.person;

import java.util.List;

public class Person {
    private String firstName;
    private String lastName;
    private String favouriteAnimal;
    private Gender gender;
    private int age;
    private double shoeSize;
    private List<String> favouriteCarpets;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFavouriteAnimal() {
        return favouriteAnimal;
    }

    public void setFavouriteAnimal(String favouriteAnimal) {
        this.favouriteAnimal = favouriteAnimal;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getShoeSize() {
        return shoeSize;
    }

    public void setShoeSize(double shoeSize) {
        this.shoeSize = shoeSize;
    }

    public List<String> getFavouriteCarpets() {
        return favouriteCarpets;
    }

    public void setFavouriteCarpets(List<String> favouriteCarpets) {
        this.favouriteCarpets = favouriteCarpets;
    }
}
