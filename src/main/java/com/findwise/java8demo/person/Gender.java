package com.findwise.java8demo.person;

import java.util.Arrays;
import java.util.List;

public enum Gender {
    UNDEFINED,
    FEMALE,
    MALE;

    public static Gender getRandomGender() {
        List<Gender> genderList = Arrays.asList(values());

        return genderList.get((int)(Math.random() * genderList.size()));
    }
}
