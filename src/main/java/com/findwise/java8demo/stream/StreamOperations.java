package com.findwise.java8demo.stream;

import com.findwise.java8demo.person.Person;
import com.findwise.java8demo.person.PersonGenerator;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamOperations {

    private List<Person> persons = new PersonGenerator().getRandomPersons(100L);

    public Set<String> getUniqueCarpetTypes() {
        return persons.stream()
                .flatMap(person -> person.getFavouriteCarpets().stream())
                .collect(Collectors.toSet());
    }

    public List<Map.Entry<String, Long>> getTopFavouriteCarpets() {
        return persons.stream()
                .filter(person -> person.getAge() < 20)
                .flatMap(person -> person.getFavouriteCarpets().stream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .sorted((firstEntry, secondEntry) -> Long.compare(secondEntry.getValue(), firstEntry.getValue()))
                .limit(5)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        StreamOperations streamOperations = new StreamOperations();

        streamOperations.getUniqueCarpetTypes()
                .forEach(System.out::println);

        streamOperations.getTopFavouriteCarpets().stream()
                .forEach(entry -> System.out.printf("%s: %d%n", entry.getKey(), entry.getValue()));
    }
}
